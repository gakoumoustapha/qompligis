Créer le fichier de configuration de conformité
===============================================

Utiliser l'icône |create_config_logo| pour ouvrir l'assistant de configuration :

.. image:: ../_static/images/fr/create_1.png
   :width: 50%

#. Choisir le format du jeu de données parmi les types supportés
#. Sélectionner le jeu de données (ou le dossier dans le cas d'un jeu de shapefile(s))
#. :guilabel:`Suivant`

.. image:: ../_static/images/fr/create_2.png
   :width: 60%

#. Couches trouvées dans le jeu de données de référence
#. Vérifications générales pour la couche sélectionnée
#. Champs de la couche sélectionnée avec des vérifications pouvant être activées
#. Vérification de règles de géométrie et de topologie (géométrie valide, géométrie nulle, géométrie vide, duplicats, superpositions)
#. Vérifications propres au type de géométrie de la couche sélectionnée

.. image:: ../_static/images/fr/create_3.png
   :width: 45%

#. Choisir un chemin où enregistrer le fichier de configuration
#. :guilabel:`Terminer`

Il est possible d'éditer un fichier de configuration existant avec l'icône |edit_config_logo|
puis en chargeant le fichier à éditer.

Au chargement de la configuration, un message de notification apparaîtra si au moins 1 champ a été ajouté/supprimé
sur l'une des couches identifiées dans le fichier de configuration. Le message liste le(s) champ(s) ajouté(s)/supprimé(s) par couche.

Si moins de 5 champs ont été ajoutés pour une couche, le message listera chaque champ.
En revanche, si plus de 5 champs ont été ajoutés, seuls les 5 premiers seront listés. Le nombre de champs supplémentaires concerné sera indiqué.

Le même principe est appliqué pour les champs supprimés.

.. image:: ../_static/images/fr/edit_1.png
   :width: 45%


.. |create_config_logo| image:: ../../QompliGIS/resources/images/newConfig.svg
   :width: 30 px

.. |edit_config_logo| image:: ../../QompliGIS/resources/images/editConfig.svg
   :width: 30 px
