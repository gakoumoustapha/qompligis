# Documentation
# -------------

furo==2022.*
myst-parser[linkify]>=0.17,<0.18
sphinx-autobuild==2021.*
sphinx-copybutton>=0.3,<1
sphinxext-opengraph>=0.4,<1
sphinx-panels
